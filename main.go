package main

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/valyala/ybc/bindings/go/ybc"
	"io"
	"io/ioutil"
	"launchpad.net/goyaml"
	"log"
	"net"
	"net/http"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var (
	bHelp = flag.Bool("h", false, "usage help")
	cConf = flag.String("c", "/etc/pkgcached.json", "conf file path")
	l     *log.Logger
	cli   http.Client
)

type Proxy struct {
	LogFile string `yaml:"logfile"`

	Cache struct {
		ObjectSize  int64  `yaml:"object_size"`
		Path        string `yaml:"path"`
		ObjectCount int    `yaml:"object_count"`
		Capacity    int    `yaml:"capacity"`
	} `yaml:"cache"`

	Server struct {
		Listen       string        `yaml:"listen"`
		ReadTimeout  time.Duration `yaml:"read_timeout"`
		WriteTimeout time.Duration `yaml:"write_timeout"`
		Workers      int           `yaml:"workers"`
		ACL          []string      `yaml:"acl"`
	} `yaml:"server"`

	Client struct {
		ReadTimeout    time.Duration `yaml:"read_timeout"`
		WriteTimeout   time.Duration `yaml:"write_timeout"`
		ConnectTimeout time.Duration `yaml:"connect_timeout"`
		Workers        int           `yaml:"workers"`
	} `yaml:"client"`

	Backends []struct {
		Prefix  string   `yaml:"prefix"`
		Servers []string `yaml:"servers"`
	} `yaml:"backends"`

	Objects []struct {
		Name   string        `yaml:"name"`
		TTL    time.Duration `yaml:"ttl,omitempty"`
		Regexp []string      `yaml:"regexp"`
	} `yaml:"objects"`

	cache ybc.Cacher
}

type Listener struct {
	net.Listener
}

func (nl Listener) Accept() (net.Conn, error) {
	var ok bool = false
	c, err := nl.Listener.Accept()

	host, port, err := net.SplitHostPort(c.RemoteAddr().String())
	if err != nil {
		c.Close()
		return nil, err
	}

	ip := net.ParseIP(host)

	for _, cidr := range conf.Server.ACL {
		if _, ipnet, err := net.ParseCIDR(cidr); err == nil {
			if ipnet.Contains(ip) {
				ok = true
			}
		}
	}
	if !ok {
		c.Close()
		return nil, fmt.Errorf("deny conn from: %s:%s\n", host, port)
	}

	l.Printf("allow conn from: %s:%s\n", host, port)
	return c, err
}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	wH := w.Header()
	wH.Set("Server", "pkgcached")

	switch r.Method {
	case "GET", "HEAD":
		break
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	rH := r.Header
	if rH.Get("If-None-Match") != "" {
		wH.Set("ETag", "W/\"CacheForever\"")
		// Set content-length as a workaround for https://code.google.com/p/go/issues/detail?id=6157
		wH.Set("Content-Length", "123")
		w.WriteHeader(http.StatusNotModified)
		return
	}

	var key []byte
	parts := strings.Split(r.RequestURI, "/")
	if parts[0] == "http:" {
		key = []byte("/" + strings.Join(parts[3:], "/"))
	} else {
		key = []byte(r.RequestURI)
	}
	l.Printf("k %s\n", key)
	item, err := p.cache.GetDeItem(key, time.Second)
	if err != nil {
		l.Printf("failed to get %s\n", err.Error())
		if err != ybc.ErrCacheMiss {
			l.Printf("Unexpeced error=[%s] when obtaining cache value by key=[%s]\n", err, key)
		}

		item = p.fetchFromUpstream(w, key)
		if item == nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}
	}
	defer item.Close()

	contentType, err := loadContentType(item)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	statusCode, err := loadStatusCode(item)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//	wH.Set("Cache-Control", "public, max-age=31536000")
	wH.Set("Content-Length", fmt.Sprintf("%d", item.Available()))
	wH.Set("Content-Type", contentType)
	w.WriteHeader(statusCode)
	if _, err = io.Copy(w, item); err != nil {
		log.Printf("Error=[%s] when sending value for key=[%s] to client\n", err, key)
	}
}

func (p *Proxy) fetchFromUpstream(w http.ResponseWriter, bytePrefix []byte) *ybc.Item {
	var hosts []string
	var key []byte
	var uri string

	for _, backend := range p.Backends {
		if strings.HasPrefix(string(bytePrefix), backend.Prefix) {
			key = bytePrefix
			uri = strings.TrimPrefix(string(bytePrefix), backend.Prefix)
			hosts = backend.Servers
		}
	}

	if len(hosts) == 0 {
		return nil
	}

	l.Printf("store k %s\n", key)
	for _, upstream := range hosts {
		requestUrl := fmt.Sprintf("http://%s%s", upstream, uri)

		l.Printf("url %s\n", requestUrl)

		resp, err := cli.Head(requestUrl)
		if err != nil {
			if resp != nil && resp.Body != nil {
				resp.Body.Close()
			}
			l.Printf("Error=[%s] when doing request to %s\n", err, requestUrl)
			w.WriteHeader(http.StatusBadRequest)
			return nil
		}

		if resp.ContentLength > p.Cache.ObjectSize {
			l.Printf("Error=[] %d > %d when doing request to %s\n", resp.ContentLength, p.Cache.ObjectSize, requestUrl)
			return nil
		}

		resp, err = cli.Get(requestUrl)
		if err != nil {
			log.Printf("Error=[%s] when doing request to %s\n", err, requestUrl)
			return nil
		}

		defer resp.Body.Close()
		buf := new(bytes.Buffer)
		defer buf.Reset()
		contentLength := resp.Header.Get("Content-Length")
		if contentLength == "" {
			log.Printf("Cannot cache response for requestUrl=[%s] without content-length try to calc it\n", requestUrl)
			if _, err := io.Copy(buf, resp.Body); err != nil {
				return nil
			} else {
				contentLength = fmt.Sprintf("%d", buf.Len())
			}
		}
		contentLengthN, err := strconv.Atoi(contentLength)
		if err != nil {
			log.Printf("Error=[%s] when parsing contentLength=[%s] for request to [%s]\n", err, contentLength, requestUrl)
			return nil
		}

		contentType := resp.Header.Get("Content-Type")
		if contentType == "" {
			contentType = "application/octet-stream"
		}

		//TODO
		var ttl time.Duration
		for _, object := range p.Objects {
			for _, reg := range object.Regexp {
				if ok, err := regexp.Match(reg, key); err == nil && ok {
					ttl = object.TTL
				} else {
					ttl = p.Objects[0].TTL
				}
			}
		}
		itemSize := contentLengthN + len(contentType) + 2
		txn, err := p.cache.NewSetTxn(key, itemSize, ttl)
		if err != nil {
			log.Printf("Error=[%s] when starting set txn for key=[%s]. itemSize=[%d]\n", err, key, itemSize)
			return nil
		}

		if err = storeContentType(txn, contentType); err != nil {
			log.Printf("Cannot store content-type for key=[%s] in cache\n", key)
			txn.Rollback()
			return nil
		}

		if err = storeStatusCode(txn, resp.StatusCode); err != nil {
			log.Printf("Cannot store status code for key=[%s] in cache\n", key)
			txn.Rollback()
			return nil
		}

		var r io.Reader

		if buf.Len() > 0 {
			r = buf
		} else {
			r = resp.Body
		}
		n, err := txn.ReadFrom(r)
		if err != nil {
			log.Printf("Error=[%s] when copying body with size=%d to cache. key=[%s]\n", err, contentLengthN, key)
			txn.Rollback()
			return nil
		}
		if n != int64(contentLengthN) {
			log.Printf("Unexpected number of bytes copied=%d from response to requestUrl=[%s]. Expected %d\n", n, requestUrl, contentLengthN)
			txn.Rollback()
			return nil
		}
		item, err := txn.CommitItem()
		if err != nil {
			log.Printf("Error=[%s] when commiting set txn for key=[%s]\n", err, key)
			return nil
		}
		return item
	}
	return nil
}

func storeStatusCode(w io.Writer, statusCode int) (err error) {
	if _, err = w.Write([]byte{byte(statusCode)}); err != nil {
		log.Printf("Error=[%s] when storing status code\n", err)
		return
	}
	return
}

func loadStatusCode(r io.Reader) (statusCode int, err error) {
	var sizeBuf [1]byte
	if _, err = r.Read(sizeBuf[:]); err != nil {
		log.Printf("Error=[%s] when extracting status code\n", err)
		return 0, err
	}
	return int(sizeBuf[0]), nil
}

func storeContentType(w io.Writer, contentType string) (err error) {
	strBuf := []byte(contentType)
	strSize := len(strBuf)
	if strSize > 255 {
		log.Printf("Too long content-type=[%s]. Its length=%d should fit one byte\n", contentType, strSize)
		err = fmt.Errorf("Too long content-type")
		return
	}
	var sizeBuf [1]byte
	sizeBuf[0] = byte(strSize)
	if _, err = w.Write(sizeBuf[:]); err != nil {
		log.Printf("Error=[%s] when storing content-type length\n", err)
		return
	}
	if _, err = w.Write(strBuf); err != nil {
		log.Printf("Error=[%s] when writing content-type string with length=%d\n", err, strSize)
		return
	}
	return
}

func loadContentType(r io.Reader) (contentType string, err error) {
	var sizeBuf [1]byte
	if _, err = r.Read(sizeBuf[:]); err != nil {
		log.Printf("Error=[%s] when extracting content-type length\n", err)
		return
	}
	strSize := int(sizeBuf[0])
	strBuf := make([]byte, strSize)
	if _, err = r.Read(strBuf); err != nil {
		log.Printf("Error=[%s] when extracting content-type string with length=%d\n", err, strSize)
		return
	}
	contentType = string(strBuf)
	return
}

var conf Proxy

func main() {
	flag.Parse()

	if *bHelp {
		flag.PrintDefaults()
		os.Exit(0)
	}

	if *cConf != "" {
		f, err := os.Open(*cConf)
		if err != nil {
			log.Fatalf(err.Error())
		} else {
			if buf, err := ioutil.ReadAll(f); err != nil {
				log.Fatalf(err.Error())
			} else {
				if err := goyaml.Unmarshal(buf, &conf); err != nil {
					log.Fatalf(err.Error())
				}
			}
		}
	}

	l = log.New(os.Stdout, "", log.LstdFlags)

	l.Printf("%+v\n", conf)
	if conf.Server.Workers < 1 {
		conf.Server.Workers = runtime.NumCPU()
	}
	runtime.GOMAXPROCS(conf.Server.Workers)

	//conf.Items = sort.Strings(conf.Items)

	ybcConfig := ybc.Config{
		MaxItemsCount: ybc.SizeT(conf.Cache.ObjectCount),
		DataFileSize:  ybc.SizeT(conf.Cache.Capacity),
	}

	var cache ybc.Cacher
	var err error
	var al Listener

	cacheFilesPath_ := strings.Split(conf.Cache.Path, ",")
	cacheFilesCount := len(cacheFilesPath_)
	l.Printf("Opening data files. This can take a while for the first time if files are big\n")
	if cacheFilesCount < 2 {
		if cacheFilesPath_[0] != "" {
			ybcConfig.DataFile = cacheFilesPath_[0] + "pkgcached.data"
			ybcConfig.IndexFile = cacheFilesPath_[0] + "pkgcached.index"
		}
		cache, err = ybcConfig.OpenCache(true)
		if err != nil {
			l.Fatalf("Cannot open cache: [%s]", err)
		}
	} else if cacheFilesCount > 1 {
		ybcConfig.MaxItemsCount /= ybc.SizeT(cacheFilesCount)
		ybcConfig.DataFileSize /= ybc.SizeT(cacheFilesCount)
		var configs ybc.ClusterConfig
		configs = make([]*ybc.Config, cacheFilesCount)
		for i := 0; i < cacheFilesCount; i++ {
			cfg := ybcConfig
			cfg.DataFile = cacheFilesPath_[i] + "pkgcached.data"
			cfg.IndexFile = cacheFilesPath_[i] + "pkgcached.index"
			configs[i] = &cfg
		}
		cache, err = configs.OpenCluster(true)
		if err != nil {
			l.Fatalf("Cannot open cache cluster: [%s]", err)
		}
	}
	defer cache.Close()
	l.Printf("Data files have been opened\n")

	conf.cache = cache

	nl, err := net.Listen("tcp", conf.Server.Listen)
	if err != nil {
		l.Printf(err.Error())
		os.Exit(1)
		return
	}
	al.Listener = nl
	//	r := http.NewServeMux()
	//r.HandleFunc("/", h)
	//http.Handle("/", r)

	s := &http.Server{
		Addr:           conf.Server.Listen,
		Handler:        &conf,
		ReadTimeout:    conf.Server.ReadTimeout,
		WriteTimeout:   conf.Server.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	cli = http.Client{
		Transport: &http.Transport{
			Dial:                timeoutDialler(),
			DisableKeepAlives:   false,
			MaxIdleConnsPerHost: conf.Client.Workers,
		},
		CheckRedirect: redirectPolicyFunc,
	}

	go s.Serve(al)

	select {}

}

func redirectPolicyFunc(req *http.Request, via []*http.Request) error {
	if len(via) >= 10 {
		return fmt.Errorf("stopped after 10 redirects")
	}
	return nil
}

func timeoutDialler() func(net, addr string) (c net.Conn, err error) {
	return func(netw, addr string) (net.Conn, error) {
		c, err := net.DialTimeout(netw, addr, conf.Client.ConnectTimeout)
		if err != nil {
			return nil, err
		}
		c.SetWriteDeadline(time.Now().Add(conf.Client.WriteTimeout))
		c.SetReadDeadline(time.Now().Add(conf.Client.ReadTimeout))
		return c, nil
	}
}
